package ru.t1.oskinea.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
