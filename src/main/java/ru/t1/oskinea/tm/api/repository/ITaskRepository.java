package ru.t1.oskinea.tm.api.repository;

import ru.t1.oskinea.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    Task create(String name);

    Task create(String name, String description);

    boolean existsById(String id);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    int getSize();

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
