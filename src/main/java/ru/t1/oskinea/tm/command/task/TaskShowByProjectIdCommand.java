package ru.t1.oskinea.tm.command.task;

import ru.t1.oskinea.tm.model.Task;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.List;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Show task list by project id.";

    private static final String NAME = "task-show-by-project-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);
        renderTasks(tasks);
    }

}
