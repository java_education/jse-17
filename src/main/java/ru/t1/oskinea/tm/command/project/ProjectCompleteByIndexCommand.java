package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Complete project by index.";

    private static final String NAME = "project-complete-by-index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(index, Status.COMPLETED);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
