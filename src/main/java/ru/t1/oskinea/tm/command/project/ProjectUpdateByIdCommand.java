package ru.t1.oskinea.tm.command.project;

import ru.t1.oskinea.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    private static final String DESCRIPTION = "Update project by id.";

    private static final String NAME = "project-update-by-id";

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateById(id, name, description);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
