package ru.t1.oskinea.tm.command.task;

import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Change task status by index.";

    private static final String NAME = "task-change-status-by-index";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.print("ENTER STATUS (");
        System.out.print(Arrays.toString(Status.values()));
        System.out.print("): ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(index, status);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
