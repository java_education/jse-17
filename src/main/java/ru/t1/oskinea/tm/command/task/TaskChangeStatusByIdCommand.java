package ru.t1.oskinea.tm.command.task;

import ru.t1.oskinea.tm.enumerated.Status;
import ru.t1.oskinea.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "Change task status by id.";

    private static final String NAME = "task-change-status-by-id";

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS (");
        System.out.println(Arrays.toString(Status.values()));
        System.out.print("): ");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusById(id, status);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

}
